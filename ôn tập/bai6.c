#include<stdio.h>

void hinhtron ()
{
    float bankinh;
    printf("Nhap ban kinh hinh tron:");
    scanf("%f", &bankinh);
    const float PI = 3.14;

    if (bankinh <= 0)
    {
        printf("Ban kinh hinh tron phai lon hon 0 \n");
    }
    else{
    float P = 2 * bankinh * PI;
    printf("Chu vi hinh tron co ban kinh %0.2f la: %0.2f \n", bankinh, P);
    float S = bankinh * bankinh * PI;
    printf("Dien tich hinh tron co ban kinh %0.2f la: %0.2f \n", bankinh, S);
    }
}

int main ()
{
    hinhtron();

}